import org.gradle.api.JavaVersion.VERSION_11
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.31"
    `maven-publish`
    `java-library`
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = VERSION_11
    targetCompatibility = VERSION_11
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = VERSION_11.majorVersion
}

tasks.create("currentKonnectVersion") {
    doLast {
        println(konnectVersion())
    }
}

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("release") {
            group = "com.gitlab.jdiasamaro"
            artifactId = rootProject.name
            version = konnectVersion()

            from(components["java"])
        }
    }
}

val versions = mapOf(
    "assertK"   to "0.25",
    "mockito"   to "4.0.0",
    "moshi"     to "1.12.0",
    "wiremock"  to "1.0.2"
)

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    api("com.squareup.moshi:moshi-kotlin:${versions["moshi"]}")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
    testImplementation("com.marcinziolo:kotlin-wiremock:${versions["wiremock"]}")
    testImplementation("org.mockito.kotlin:mockito-kotlin:${versions["mockito"]}")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:${versions["assertK"]}")
}

fun konnectVersion(): String {
    val versionFile = sourceSets.main.map { main ->
        main.resources.files.first {
            it.name == "version"
        }
    }

    return versionFile.get().readLines().first()
}
