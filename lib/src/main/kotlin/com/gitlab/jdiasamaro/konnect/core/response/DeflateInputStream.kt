package com.gitlab.jdiasamaro.konnect.core.response

import java.io.InputStream
import java.io.PushbackInputStream
import java.util.zip.Inflater
import java.util.zip.InflaterInputStream
import java.util.zip.ZipException

/**
 * This input stream tries to identify the type of deflation since many
 * servers do not implement the "deflate" encoding in the same way (with the zlib wrapper).
 *
 * Adapted from the DeflateInputStream class from http-components-client Apache project
 */
internal class DeflateInputStream(wrapped: InputStream): InputStream() {

    private val sourceStream: InputStream

    init {
        val pushback = PushbackInputStream(wrapped, 2)
        val i1 = pushback.read()
        val i2 = pushback.read()
        if (i1 == -1 || i2 == -1) {
            throw ZipException("Unexpected end of stream")
        }

        pushback.unread(i2)
        pushback.unread(i1)

        var nowrap = true
        val b1 = i1 and 0xFF
        val compressionMethod = b1 and 0xF
        val compressionInfo = b1 shr 4 and 0xF
        val b2 = i2 and 0xFF

        if (compressionMethod == 8 && compressionInfo <= 7 && (b1 shl 8 or b2) % 31 == 0) {
            nowrap = false
        }

        sourceStream = DeflateStream(pushback, Inflater(nowrap))
    }

    override fun read(): Int {
        return sourceStream.read()
    }

    override fun read(b: ByteArray): Int {
        return sourceStream.read(b)
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        return sourceStream.read(b, off, len)
    }

    override fun skip(n: Long): Long {
        return sourceStream.skip(n)
    }

    override fun available(): Int {
        return sourceStream.available()
    }

    override fun mark(readLimit: Int) {
        sourceStream.mark(readLimit)
    }

    override fun reset() {
        sourceStream.reset()
    }

    override fun markSupported(): Boolean {
        return sourceStream.markSupported()
    }

    override fun close() {
        sourceStream.close()
    }
}

private class DeflateStream(
    inputStream: InputStream,
    inflater: Inflater
) : InflaterInputStream(inputStream, inflater) {

    private var closed = false

    override fun close() {
        if (closed) {
            return
        }

        closed = true
        inf.end()

        super.close()
    }
}
