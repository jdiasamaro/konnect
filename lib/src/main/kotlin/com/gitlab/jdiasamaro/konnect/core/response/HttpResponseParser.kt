package com.gitlab.jdiasamaro.konnect.core.response

import com.gitlab.jdiasamaro.konnect.KonnectJsonEngine
import com.gitlab.jdiasamaro.konnect.KonnectTypeContext
import java.io.BufferedReader
import java.io.InputStream

class HttpResponseParser(private val json: KonnectJsonEngine) {

    fun deserialize(inputStream: InputStream, type: KonnectTypeContext<*>): Any? {
        return when (type.rawClass) {
            Unit::class.java -> Unit
            InputStream::class.java -> inputStream
            String::class.java -> asText(inputStream)
            Int::class.java -> asText(inputStream).toInt()
            Long::class.java -> asText(inputStream).toLong()
            Boolean::class.java -> asText(inputStream).toBoolean()
            else -> json.fromJson(inputStream, type)
        }
    }

    private fun asText(inputStream: InputStream): String {
        return inputStream.bufferedReader().use(BufferedReader::readText)
    }
}