package com.gitlab.jdiasamaro.konnect.configs

import com.gitlab.jdiasamaro.konnect.KonnectTypeContext
import com.gitlab.jdiasamaro.konnect.buildReifiedTypeContext
import com.gitlab.jdiasamaro.konnect.emptyTypeContext

data class RequestBuilder(
    var bodySupplier: () -> Any? = { null },
    val headers: RequestHeadersBuilder = RequestHeadersBuilder(),
    var bodyTypeContext: KonnectTypeContext<*> = emptyTypeContext,
    val compression: RequestCompressionConfig = RequestCompressionConfig(),
    val queryParams: RequestQueryParametersBuilder = RequestQueryParametersBuilder()
) {
    inline fun <reified T> body(noinline supplier: () -> T?) {
        bodySupplier = supplier
        bodyTypeContext = buildReifiedTypeContext<T>()
    }

    infix fun RequestCompressionConfig.accept(compressionValue: HttpRequestCompression) {
        values.clear()
        values.add(compressionValue)
    }

    infix fun RequestCompressionConfig.accept(compressionValue: List<HttpRequestCompression>) {
        values.clear()
        values.addAll(compressionValue)
    }

    fun headers(block: RequestHeadersBuilder.() -> Unit) {
        block.invoke(headers)
    }

    fun queryParameters(block: RequestQueryParametersBuilder.() -> Unit) {
        block.invoke(queryParams)
    }
}