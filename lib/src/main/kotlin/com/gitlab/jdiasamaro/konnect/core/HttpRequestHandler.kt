package com.gitlab.jdiasamaro.konnect.core

import com.gitlab.jdiasamaro.konnect.KonnectJsonEngine
import com.gitlab.jdiasamaro.konnect.configs.KonnectConfigs
import com.gitlab.jdiasamaro.konnect.configs.RequestBuilder
import com.gitlab.jdiasamaro.konnect.core.json.MoshiJsonEngine
import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_ENCODING
import java.net.URISyntaxException
import java.net.URL
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpClient.Redirect.NORMAL
import java.net.http.HttpClient.Version.HTTP_2
import java.net.http.HttpRequest
import java.net.http.HttpResponse.BodyHandlers.ofInputStream

private val HTTP_CLIENT = HttpClient.newBuilder()
    .followRedirects(NORMAL)
    .version(HTTP_2)
    .build()

class HttpRequestHandler(
    private val url: String,
    private val method: String,
    private val konnectConfigs: KonnectConfigs,
    private val requestConfigBuilder: (RequestBuilder.() -> Unit)?,
    private val json: KonnectJsonEngine = MoshiJsonEngine(konnectConfigs.moshiJsonAdapterFactories)
) {

    fun sendRequest(): HttpResponse {
        val uri = UrlBuilder(konnectConfigs.baseUrl, url).build()
        val baseRequestConfigs = buildRequestConfigs(requestConfigBuilder)
        val mergedQueryString = buildMergedQueryString(uri, baseRequestConfigs)
        val (bodyPublisher, requestConfigs) = BodyPublisherBuilder(json, baseRequestConfigs).build()

        val requestBuilder = HttpRequest.newBuilder().method(method, bodyPublisher).apply {
            val url = replaceQueryString(uri, mergedQueryString)
            setRequestHeaders(requestConfigs)
            uri(url.toURI())
        }

        val httpRequest = requestBuilder.build()
        val httpResponse = try {
            HTTP_CLIENT.send(httpRequest, ofInputStream())
        } catch (e: Exception) {
            throw KonnectException("Error while sending the HTTP request", e)
        }

        return HttpResponse(json, httpResponse)
    }

    private fun replaceQueryString(url: URL, queryString: String): URL {
        val urlString = "${url.protocol}://${url.authority}${url.path}".let {
            if (queryString.isBlank()) {
                it
            } else {
                "${it}?${queryString}"
            }
        }

        return URL(urlString)
    }

    private fun buildRequestConfigs(requestBuilder: (RequestBuilder.() -> Unit)? = null): RequestBuilder {
        val requestConfigs = RequestBuilder(
            headers = konnectConfigs.defaults.headers.copy(),
            compression = konnectConfigs.defaults.compression.copy(),
            queryParams = konnectConfigs.defaults.queryParams.copy()
        )

        return requestConfigs.also {
            requestBuilder?.invoke(it)
        }
    }

    private fun HttpRequest.Builder.setRequestHeaders(requestConfig: RequestBuilder) {
        val acceptEncodingValue = requestConfig.compression.values
            .mapNotNull { it.encoding }
            .joinToString(separator = ",")
            .ifBlank { null }

        acceptEncodingValue?.let {
            header(ACCEPT_ENCODING, acceptEncodingValue)
        }

        requestConfig.headers.forEach {
            it.value.forEach { value ->
                header(it.key, value)
            }
        }
    }

    private fun buildMergedQueryString(uri: URL, requestConfig: RequestBuilder): String {
        val baseQueryParams = if (uri.query == null) {
            emptyMap()
        } else {
            try {
                uri.query
                    .split("&")
                    .map {
                        val tokens = it.split("=")
                        tokens[0] to tokens[1]
                    }.groupBy {
                        it.first
                    }.mapValues { entry ->
                        entry.value.map { it.second }
                    }
            } catch (e: Exception) {
                throw KonnectException(cause = URISyntaxException(uri.query, "Invalid query structure"))
            }
        }

        val allQueryParamKeys = baseQueryParams.keys union requestConfig.queryParams.keys
        val mergedQueryParams = allQueryParamKeys.associateWith {
            val baseParamValues = baseQueryParams.getOrDefault(it, emptyList())
            val extraParamValues = requestConfig.queryParams.getOrDefault(it, emptyList())
            baseParamValues + extraParamValues
        }

        val encodedQueryParams = mergedQueryParams.map { param ->
            param.value.map {
                "${URLEncoder.encode(param.key, "UTF-8")}=${URLEncoder.encode(it, "UTF-8")}"
            }
        }

        return encodedQueryParams.flatten().joinToString(separator = "&")
    }
}