package com.gitlab.jdiasamaro.konnect.core.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonReader.Token.NULL
import com.squareup.moshi.JsonReader.Token.STRING
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

internal class ToStringBasedAdapter<T>(private val fromStringProvider: (String) -> T): JsonAdapter<T>() {

    @FromJson
    @Synchronized
    override fun fromJson(reader: JsonReader): T? {
        return when (reader.peek()) {
            NULL -> reader.nextNull()
            STRING -> fromStringProvider.invoke(reader.nextString())
            else -> throw JsonDataException("Expected a string or null value but found ${reader.peek()} at path: ${reader.path}")
        }
    }

    @ToJson
    @Synchronized
    override fun toJson(writer: JsonWriter, value: T?) {
        if (value != null) {
            writer.value(value.toString())
        } else {
            writer.nullValue()
        }
    }
}