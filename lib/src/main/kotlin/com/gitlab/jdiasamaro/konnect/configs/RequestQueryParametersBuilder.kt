package com.gitlab.jdiasamaro.konnect.configs

data class ParamKey(val key: String)

data class RequestQueryParametersBuilder(
    private val queryParams: MutableMap<String, MutableList<String>> = mutableMapOf()
): Map<String, List<String>> by queryParams {

    fun param(key: String) = ParamKey(key)

    infix fun <T> ParamKey.add(value: T) = add(listOf(value))
    infix fun <T> ParamKey.add(values: Iterable<T>) {
        queryParams.getOrPut(key) {
            ArrayList()
        }.apply {
            addAll(values.map {
                paramToString(it)
            })
        }
    }

    infix fun <T> ParamKey.set(value: T) = set(listOf(value))

    infix fun <T> ParamKey.set(values: Iterable<T>) {
        queryParams[key] = values.mapTo(ArrayList()) {
            paramToString(it)
        }
    }
}

private fun <T> paramToString(value: T): String = when (value) {
    is Double -> value.toBigDecimal().toPlainString()
    else -> value.toString()
}