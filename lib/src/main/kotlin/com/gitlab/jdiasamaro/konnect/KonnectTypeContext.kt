package com.gitlab.jdiasamaro.konnect

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * A (de)serialization context of a given type (both parametrized and raw types)
 */
data class KonnectTypeContext<T>(
    /**
     * Providers must ensure this field contains all generic type parameters (for instance, for JSON handling)
     * of the raw type
     */
    val typeParameters: List<Type>,

    /**
     * To allow safe casting to T
     */
    val rawClass: Class<T>,

    /**
     * Raw type defined by T
     */
    val rawType: Type,

    /**
     * @return if this type is marked as a Kotlin nullable type
     */
    val isNullable: Boolean
)

inline fun <reified T> buildReifiedTypeContext(): KonnectTypeContext<T> {
    val typeCapture = object: TypeCapture<T>() { }

    return KonnectTypeContext(
        typeParameters = typeCapture.typeParameters,
        rawType = typeCapture.rawType,
        rawClass = T::class.java,
        isNullable = null is T,
    )
}

abstract class TypeCapture<T> {
    val typeParameters: List<Type>
    val rawType: Type

    init {
        val superClass = this.javaClass.genericSuperclass
        rawType = if (superClass is ParameterizedType) {
            superClass.actualTypeArguments.first()
        } else {
            throw IllegalArgumentException("TypeCapture needs to be called with a defined parameter type. Example: TypeCapture<List<String>>() { }")
        }

        typeParameters = if (rawType is ParameterizedType) {
            rawType.actualTypeArguments.toList()
        } else {
            emptyList()
        }
    }
}

val emptyTypeContext = KonnectTypeContext(
    typeParameters = emptyList(),
    rawClass = Any::class.java,
    rawType = Any::class.java,
    isNullable = false
)