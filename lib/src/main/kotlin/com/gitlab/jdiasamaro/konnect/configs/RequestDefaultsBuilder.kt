package com.gitlab.jdiasamaro.konnect.configs

import com.gitlab.jdiasamaro.konnect.KONNECT_VERSION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.USER_AGENT

private fun defaultRequestHeaders() = RequestHeadersBuilder(mutableMapOf(
    USER_AGENT to mutableListOf("Konnect/$KONNECT_VERSION")
))

data class RequestDefaultsBuilder(
    val headers: RequestHeadersBuilder = defaultRequestHeaders(),
    val compression: RequestCompressionConfig = RequestCompressionConfig(),
    val queryParams: RequestQueryParametersBuilder = RequestQueryParametersBuilder()
) {
    infix fun RequestCompressionConfig.accept(compressionValue: HttpRequestCompression) {
        values.clear()
        values.add(compressionValue)
    }

    infix fun RequestCompressionConfig.accept(compressionValue: List<HttpRequestCompression>) {
        values.clear()
        values.addAll(compressionValue)
    }

    fun headers(block: RequestHeadersBuilder.() -> Unit) {
        block.invoke(headers)
    }

    fun queryParameters(block: RequestQueryParametersBuilder.() -> Unit) {
        block.invoke(queryParams)
    }
}