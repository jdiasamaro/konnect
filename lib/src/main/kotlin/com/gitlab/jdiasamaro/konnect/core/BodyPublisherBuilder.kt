package com.gitlab.jdiasamaro.konnect.core

import com.gitlab.jdiasamaro.konnect.KonnectJsonEngine
import com.gitlab.jdiasamaro.konnect.configs.RequestBuilder
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader
import java.io.InputStream
import java.net.http.HttpRequest
import java.net.http.HttpRequest.BodyPublisher

internal class BodyPublisherBuilder(private val json: KonnectJsonEngine, private val requestConfig: RequestBuilder) {

    fun build(): Pair<BodyPublisher, RequestBuilder> {
        val inferredContentType = mutableListOf<String>()

        val bodyPublisher = when (val body = requestConfig.bodySupplier.invoke()) {
            null -> HttpRequest.BodyPublishers.noBody()
            is String -> {
                inferredContentType.add("text/plain; charset=UTF-8")
                HttpRequest.BodyPublishers.ofString(body)
            }
            is ByteArray -> {
                inferredContentType.add("application/octet-stream")
                HttpRequest.BodyPublishers.ofByteArray(body)
            }
            is InputStream -> {
                inferredContentType.add("application/octet-stream")
                HttpRequest.BodyPublishers.ofInputStream {
                    body
                }
            }
            else -> {
                inferredContentType.add("application/json; charset=UTF-8")
                val jsonString = json.toJson(body, requestConfig.bodyTypeContext)
                HttpRequest.BodyPublishers.ofString(jsonString)
            }
        }

        val adjustedRequestConfigs = when {
            requestConfig.headers.containsKey(HttpHeader.CONTENT_TYPE) -> requestConfig
            inferredContentType.isNotEmpty() -> {
                val changedHeaders = requestConfig.headers.copy().apply {
                    contentType set inferredContentType
                }

                requestConfig.copy(headers = changedHeaders)
            }
            else -> requestConfig
        }

        return Pair(bodyPublisher, adjustedRequestConfigs)
    }
}