package com.gitlab.jdiasamaro.konnect.core

import java.net.URI
import java.net.URL

internal class UrlBuilder(private val baseUrl: String?, private val url: String) {

    fun build(): URL {
        if (baseUrl == null) {
            return try {
                URL(url)
            } catch (e: Exception) {
                throw IllegalArgumentException("Invalid URL: $url", e)
            }
        }

        val baseUri = URI(baseUrl)
        if (!baseUri.isAbsolute) {
            throw IllegalStateException("Base URL must be absolute: $baseUrl")
        }

        val uri = URI(url)
        if (uri.isAbsolute) {
            throw IllegalArgumentException("Base URL was supplied ($baseUrl). Extra URL path must not be absolute: $url")
        }

        val scheme = baseUri.scheme
        val authority = baseUri.authority

        val path = listOfNotNull(
            baseUri.rawPath.removeSuffix("/"),
            uri.rawPath.removePrefix("/").ifBlank { null }
        ).joinToString(separator = "/")

        val query = listOfNotNull(baseUri.rawQuery, uri.rawQuery).filter { it.isNotBlank() }.joinToString(separator = "&")

        val mergedUrl = "${scheme}://${authority}${path}".let {
            if (query.isBlank()) {
                it
            } else {
                "${it}?${query}"
            }
        }

        return URL(mergedUrl)
    }
}