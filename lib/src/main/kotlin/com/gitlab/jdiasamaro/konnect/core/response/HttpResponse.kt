package com.gitlab.jdiasamaro.konnect.core.response

import com.gitlab.jdiasamaro.konnect.KonnectJsonEngine
import com.gitlab.jdiasamaro.konnect.buildReifiedTypeContext
import com.gitlab.jdiasamaro.konnect.configs.DEFLATE_ENCODING
import com.gitlab.jdiasamaro.konnect.configs.GZIP_ENCODING
import com.gitlab.jdiasamaro.konnect.core.KonnectException
import com.gitlab.jdiasamaro.konnect.core.KonnectExceptionType.DESERIALIZATION_ERROR
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_ENCODING
import java.io.InputStream
import java.net.http.HttpResponse
import java.util.zip.GZIPInputStream
import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse as KonnectHttpResponse

class HttpResponse(json: KonnectJsonEngine, val internal: HttpResponse<InputStream>): AutoCloseable {

    val headers by lazy<Map<String, List<String>>> {
        internal.headers()?.map() ?: emptyMap()
    }

    val status = internal.statusCode()
    val body = HttpResponseBodyHandler(json, this)

    fun isSuccess() = status in 200..299
    fun isRedirect() = status in 300..399
    fun isClientError() = status in 400..499
    fun isServerError() = status in 500..599

    /**
     * Convenience method to convert the response body into a UTF-8 String and close the response stream as well.
     * The response body is converted from its byte form into a String, regardless if the status code is a success
     * or not.
     */
    fun toText() = use {
        body.asText()
    }

    /**
     * This method is better used in a context where a 2XX status is assumed, avoiding the need to wrap
     * the response body converters inside a closeable call, as it closes the response stream automatically.
     * However, if the response body is not a JSON object, an exception will be thrown.
     */
    fun toJsonObject() = use {
        body.asJsonObject()
    }

    /**
     * This method is better used in a context where a 2XX status is assumed, avoiding the need to wrap
     * the response body converters inside a closeable call, as it closes the response stream automatically.
     * However, if the response body is not a JSON array, an exception will be thrown.
     */
    fun toJsonArray() = use {
        body.asJsonArray()
    }

    /**
     * Consumes the response body and returns it as a conversion to the specified type.
     * If the response type is compatible with a JSON representation of the supplied type, it will
     * be deserialized into it. Also, this method closes the response stream automatically.
     */
    inline fun <reified T> toValue() = use {
        body.asValue<T>()
    }

    override fun close() {
        internal.body().close()
    }
}

class HttpResponseBodyHandler(val json: KonnectJsonEngine, val httpResponse: KonnectHttpResponse) {

    val bodyStream: InputStream by lazy {
        val responseEncodings = httpResponse.headers[CONTENT_ENCODING]?.let { encodings ->
            val allEncodings = encodings.joinToString(separator = ",").split(",").reversed()
            allEncodings.filter { it.isNotBlank() }.map { it.trim().lowercase() }
        }

        val contentEncoding = responseEncodings ?: emptyList()
        var stream = httpResponse.internal.body()

        contentEncoding.forEach {
            when (it) {
                GZIP_ENCODING -> stream = GZIPInputStream(stream)
                DEFLATE_ENCODING -> stream = DeflateInputStream(stream)
            }
        }

        stream
    }

    fun asText(): String {
        return asValue()
    }

    fun asStream(): InputStream {
        return bodyStream
    }

    fun asJsonObject(): Map<String, Any?> {
        return asValue()
    }

    fun asJsonArray(): List<Any?> {
        return asValue()
    }

    inline fun <reified T> asValue(): T {
        return try {
            val type = buildReifiedTypeContext<T>()
            val result = HttpResponseParser(json).deserialize(bodyStream, type)
            type.rawClass.cast(result).also {
                if (!type.isNullable && it == null) {
                    throw KonnectException("${type.rawType} is not nullable but deserialized value is null")
                }
            }
        } catch (e: Exception) {
            throw KonnectException(
                message = "Error while deserializing response value to desired type: ${T::class.java}",
                type = DESERIALIZATION_ERROR,
                httpResponse = httpResponse,
                cause = e
            )
        }
    }
}