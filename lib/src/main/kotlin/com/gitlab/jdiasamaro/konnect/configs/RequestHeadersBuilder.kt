package com.gitlab.jdiasamaro.konnect.configs

import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_CHARSET
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_LANGUAGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_RANGES
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_CREDENTIALS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_HEADERS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_METHODS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_ORIGIN
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_REQUEST_HEADERS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_ALLOW_REQUEST_METHOD
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_EXPOSE_HEADERS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCESS_CONTROL_MAX_AGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.AGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ALLOW
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ALPN
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.AUTHENTICATION_INFO
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.AUTHORIZATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CACHE_CONTROL
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_DISPOSITION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_LANGUAGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_LOCATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_RANGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_TYPE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.COOKIE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.DESTINATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.EXPIRES
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.E_TAG
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.FORWARDED
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.HTTP_2_SETTINGS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_MATCH
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_MODIFIED_SINCE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_NONE_MATCH
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_RANGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_SCHEDULE_TAG_MATCH
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.IF_UNMODIFIED_SINCE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.LAST_MODIFIED
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.LINK
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.LOCATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.LOCK_TOKEN
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.MAX_FORWARDS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.MIME_VERSION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ORDERING_TYPE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.OVERWRITE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.POSITION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PRAGMA
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PREFER
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PREFERENCE_APPLIED
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PROXY_AUTHENTICATE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PROXY_AUTHENTICATION_INFO
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PROXY_AUTHORIZATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PUBLIC_KEY_PINS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.PUBLIC_KEY_PINS_REPORT_ONLY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.RANGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.RETRY_AFTER
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SCHEDULE_REPLY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SCHEDULE_TAG
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SEC_WEB_SOCKET_ACCEPT
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SEC_WEB_SOCKET_EXTENSIONS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SEC_WEB_SOCKET_KEY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SEC_WEB_SOCKET_PROTOCOL
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SEC_WEB_SOCKET_VERSION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SERVER
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SET_COOKIE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.SLUG
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.STRICT_TRANSPORT_SECURITY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.TE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.TIMEOUT
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.TRAILER
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.TRANSFER_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.USER_AGENT
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.VARY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.WWW_AUTHENTICATE

data class HeaderKey(val key: String)

data class RequestHeadersBuilder(
    private val headers: MutableMap<String, MutableList<String>> = mutableMapOf()
): Map<String, List<String>> by headers {

    fun header(key: String) = HeaderKey(key)

    infix fun <T> HeaderKey.add(value: T) = add(listOf(value))
    infix fun <T> HeaderKey.add(values: Iterable<T>) {
        headers.getOrPut(key) {
            ArrayList()
        }.apply {
            addAll(values.map {
                headerToString(it)
            })
        }
    }

    infix fun <T> HeaderKey.set(value: T) = set(listOf(value))

    infix fun <T> HeaderKey.set(values: Iterable<T>) {
        headers[key] = values.mapTo(ArrayList()) {
            headerToString(it)
        }
    }

    val accept = header(ACCEPT)
    val acceptCharset = header(ACCEPT_CHARSET)
    val acceptEncoding = header(ACCEPT_ENCODING)
    val acceptLanguage = header(ACCEPT_LANGUAGE)
    val acceptRanges = header(ACCEPT_RANGES)
    val age = header(AGE)
    val allow = header(ALLOW)
    val alpn = header(ALPN)
    val authenticationInfo = header(AUTHENTICATION_INFO)
    val authorization = header(AUTHORIZATION)
    val cacheControl = header(CACHE_CONTROL)
    val contentDisposition = header(CONTENT_DISPOSITION)
    val contentEncoding = header(CONTENT_ENCODING)
    val contentLanguage = header(CONTENT_LANGUAGE)
    val contentLocation = header(CONTENT_LOCATION)
    val contentRange = header(CONTENT_RANGE)
    val contentType = header(CONTENT_TYPE)
    val cookie = header(COOKIE)
    val destination = header(DESTINATION)
    val eTag = header(E_TAG)
    val expires = header(EXPIRES)
    val forwarded = header(FORWARDED)
    val http2Settings = header(HTTP_2_SETTINGS)
    val ifMatch = header(IF_MATCH)
    val ifModifiedSince = header(IF_MODIFIED_SINCE)
    val ifNoneMatch = header(IF_NONE_MATCH)
    val ifRange = header(IF_RANGE)
    val ifScheduleTagMatch = header(IF_SCHEDULE_TAG_MATCH)
    val ifUnmodifiedSince = header(IF_UNMODIFIED_SINCE)
    val lastModified = header(LAST_MODIFIED)
    val location = header(LOCATION)
    val lockToken = header(LOCK_TOKEN)
    val link = header(LINK)
    val maxForwards = header(MAX_FORWARDS)
    val mimeVersion = header(MIME_VERSION)
    val orderingType = header(ORDERING_TYPE)
    val overwrite = header(OVERWRITE)
    val position = header(POSITION)
    val pragma = header(PRAGMA)
    val prefer = header(PREFER)
    val preferenceApplied = header(PREFERENCE_APPLIED)
    val proxyAuthenticate = header(PROXY_AUTHENTICATE)
    val proxyAuthenticationInfo = header(PROXY_AUTHENTICATION_INFO)
    val proxyAuthorization = header(PROXY_AUTHORIZATION)
    val publicKeyPins = header(PUBLIC_KEY_PINS)
    val publicKeyPinsReportOnly = header(PUBLIC_KEY_PINS_REPORT_ONLY)
    val range = header(RANGE)
    val retryAfter = header(RETRY_AFTER)
    val scheduleReply = header(SCHEDULE_REPLY)
    val scheduleTag = header(SCHEDULE_TAG)
    val secWebSocketAccept = header(SEC_WEB_SOCKET_ACCEPT)
    val secWebSocketExtensions = header(SEC_WEB_SOCKET_EXTENSIONS)
    val secWebSocketKey = header(SEC_WEB_SOCKET_KEY)
    val secWebSocketProtocol = header(SEC_WEB_SOCKET_PROTOCOL)
    val secWebSocketVersion = header(SEC_WEB_SOCKET_VERSION)
    val server = header(SERVER)
    val setCookie = header(SET_COOKIE)
    val slug = header(SLUG)
    val strictTransportSecurity = header(STRICT_TRANSPORT_SECURITY)
    val te = header(TE)
    val timeout = header(TIMEOUT)
    val trailer = header(TRAILER)
    val transferEncoding = header(TRANSFER_ENCODING)
    val userAgent = header(USER_AGENT)
    val vary = header(VARY)
    val wwwAuthenticate = header(WWW_AUTHENTICATE)
    val accessControlAllowOrigin = header(ACCESS_CONTROL_ALLOW_ORIGIN)
    val accessControlAllowMethods = header(ACCESS_CONTROL_ALLOW_METHODS)
    val accessControlAllowCredentials = header(ACCESS_CONTROL_ALLOW_CREDENTIALS)
    val accessControlAllowHeaders = header(ACCESS_CONTROL_ALLOW_HEADERS)
    val accessControlRequestMethod = header(ACCESS_CONTROL_ALLOW_REQUEST_METHOD)
    val accessControlRequestHeaders = header(ACCESS_CONTROL_ALLOW_REQUEST_HEADERS)
    val accessControlExposeHeaders = header(ACCESS_CONTROL_EXPOSE_HEADERS)
    val accessControlMaxAge = header(ACCESS_CONTROL_MAX_AGE)
}

private fun <T> headerToString(value: T): String = when (value) {
    is Double -> value.toBigDecimal().toPlainString()
    else -> value.toString()
}