package com.gitlab.jdiasamaro.konnect.configs

import com.squareup.moshi.JsonAdapter

data class KonnectConfigs(
    /**
     * Stores the default values to be used on all requests
     */
    val defaults: RequestDefaultsBuilder = RequestDefaultsBuilder(),

    /**
     * Base URL to be prefixed on all requests
     */
    var baseUrl: String? = null,

    /**
     * A list of custom JSON adapter factories to be used internally by Moshi to (de)serialize response
     * and request types. These factories will have precedence over factories and adapters already registered.
     */
    val moshiJsonAdapterFactories: MutableList<JsonAdapter.Factory> = mutableListOf()
) {
    /**
     * Configures the default values to be used on all requests sent by
     * the configured Konnect instance
     */
    fun defaults(block: RequestDefaultsBuilder.() -> Unit) {
        block.invoke(defaults)
    }

    /**
     * Allows the inclusion of custom Moshi JSON adapter factories.
     * These factories will have precedence over factories and adapters already registered.
     */
    fun jsonAdapters(block: MutableList<JsonAdapter.Factory>.() -> Unit) {
        block.invoke(moshiJsonAdapterFactories)
    }
}