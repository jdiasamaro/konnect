package com.gitlab.jdiasamaro.konnect.utils

object HttpMethod {
    const val GET = "GET"
    const val PUT = "PUT"
    const val POST = "POST"
    const val PATCH = "PATCH"
    const val DELETE = "DELETE"
}