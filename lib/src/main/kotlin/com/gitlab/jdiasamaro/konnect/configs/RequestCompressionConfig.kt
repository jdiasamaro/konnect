package com.gitlab.jdiasamaro.konnect.configs

const val GZIP_ENCODING = "gzip"
const val DEFLATE_ENCODING = "deflate"

enum class HttpRequestCompression(val encoding: String?) {
    /**
     * No compression will be requested
     */
    NO_COMPRESSION(null),

    /**
     * Gzip encoding (supported by most servers) as described in RFC1952
     */
    GZIP(GZIP_ENCODING),

    /**
     * The "deflate" format is defined as the "deflate" compression mechanism (described in RFC1951) used
     * inside the "zlib" data format (RFC1950).
     *
     * Note: Some incorrect implementations send the "deflate" compressed data without the zlib wrapper.
     */
    DEFLATE(DEFLATE_ENCODING)
}

data class RequestCompressionConfig(val values: MutableList<HttpRequestCompression> = mutableListOf())