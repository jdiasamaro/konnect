package com.gitlab.jdiasamaro.konnect.core

import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse

enum class KonnectExceptionType {
    DESERIALIZATION_ERROR
}

open class KonnectException(
    message: String? = null,
    cause: Throwable? = null,
    val httpResponse: HttpResponse? = null,
    val type: KonnectExceptionType? = null
) : Exception(message, cause)

