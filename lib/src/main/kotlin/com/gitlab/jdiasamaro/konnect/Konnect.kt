package com.gitlab.jdiasamaro.konnect

import com.gitlab.jdiasamaro.konnect.configs.KonnectConfigs
import com.gitlab.jdiasamaro.konnect.configs.RequestBuilder
import com.gitlab.jdiasamaro.konnect.core.HttpRequestHandler
import com.gitlab.jdiasamaro.konnect.core.KonnectException
import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse
import com.gitlab.jdiasamaro.konnect.utils.HttpMethod.DELETE
import com.gitlab.jdiasamaro.konnect.utils.HttpMethod.GET
import com.gitlab.jdiasamaro.konnect.utils.HttpMethod.PATCH
import com.gitlab.jdiasamaro.konnect.utils.HttpMethod.POST
import com.gitlab.jdiasamaro.konnect.utils.HttpMethod.PUT

internal val KONNECT_VERSION = Konnect::class.java.classLoader.getResource("version").let {
    it?.readText() ?: throw KonnectException("Error finding Konnect version file")
}

class Konnect(configsBuilder: (KonnectConfigs.() -> Unit)? = null) {

    private val configs = KonnectConfigs().apply {
        configsBuilder?.invoke(this)
    }

    /**
     * Performs a GET request for the given URL and the configured defaults, definitions and request builder.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun get(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        return request(url, GET, configs, requestBuilder)
    }

    /**
     * Simplification of the get() method where the body is supplied as a parameter.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return the response value (if response status code is 2XX, throws exception otherwise)
     */
    fun get(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        val requestConfigBuilder = requestConfigsWithBody(requestBody, requestBuilder)
        return request(url, GET, configs, requestConfigBuilder)
    }

    /**
     * Performs a POST request for the given URL and the configured defaults, definitions and request builder.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun post(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        return request(url, POST, configs, requestBuilder)
    }

    /**
     * Simplification of the post() method where the body is supplied as a parameter.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return the response value (if response status code is 2XX, throws exception otherwise)
     */
    fun post(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        val requestConfigBuilder = requestConfigsWithBody(requestBody, requestBuilder)
        return request(url, POST, configs, requestConfigBuilder)
    }

    /**
     * Performs a PUT request for the given URL and the configured defaults, definitions and request builder.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun put(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        return request(url, PUT, configs, requestBuilder)
    }

    /**
     * Simplification of the put() method where the body is supplied as a parameter.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun put(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        val requestConfigBuilder = requestConfigsWithBody(requestBody, requestBuilder)
        return request(url, PUT, configs, requestConfigBuilder)
    }


    /**
     * Performs a DELETE request for the given URL and the configured defaults, definitions and request builder.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun delete(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        return request(url, DELETE, configs, requestBuilder)
    }

    /**
     * Simplification of the delete() method where the body is supplied as a parameter.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun delete(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        val requestConfigBuilder = requestConfigsWithBody(requestBody, requestBuilder)
        return request(url, DELETE, configs, requestConfigBuilder)
    }

    /**
     * Performs a PATCH request for the given URL and the configured defaults, definitions and request builder.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun patch(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        return request(url, PATCH, configs, requestBuilder)
    }

    /**
     * Simplification of the patch() method where the body is supplied as a parameter.
     * The HttpResponse object must be closed explicitly or handled in a .use block.
     *
     * @return a complete response object with info regarding: status code, response headers and body
     */
    fun patch(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
        val requestConfigBuilder = requestConfigsWithBody(requestBody, requestBuilder)
        return request(url, PATCH, configs, requestConfigBuilder)
    }

    private fun request(url: String, method: String, configs: KonnectConfigs, requestConfig: (RequestBuilder.() -> Unit)?): HttpResponse {
        val requestHandler = HttpRequestHandler(
            url = url,
            method = method,
            konnectConfigs = configs,
            requestConfigBuilder = requestConfig
        )

        return requestHandler.sendRequest()
    }

    private fun requestConfigsWithBody(requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)?) = { builder: RequestBuilder ->
        builder.body {
            requestBody
        }.also {
            requestBuilder?.invoke(builder)
        }
    }
}

/**
 * Performs a GET request with Konnect default configurations
 *
 * @see Konnect.get
 */
fun get(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().get(url, requestBuilder)
}

/**
 * Simplification of the get() method where the body is supplied as a parameter
 *
 * @see Konnect.get
 */
fun get(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().get(url, requestBody, requestBuilder)
}

/**
 * Performs a POST request with Konnect default configurations
 *
 * @see Konnect.post
 */
fun post(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().post(url, requestBuilder)
}

/**
 * Simplification of the post() method where the body is supplied as a parameter
 *
 * @see Konnect.post
 */
fun post(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().post(url, requestBody, requestBuilder)
}

/**
 * Performs a PUT request with Konnect default configurations
 *
 * @see Konnect.put
 */
fun put(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().put(url, requestBuilder)
}

/**
 * Simplification of the put() method where the body is supplied as a parameter
 *
 * @see Konnect.put
 */
fun put(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().put(url, requestBody, requestBuilder)
}

/**
 * Performs a DELETE request with Konnect default configurations
 *
 * @see Konnect.delete
 */
fun delete(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().delete(url, requestBuilder)
}

/**
 * Simplification of the delete() method where the body is supplied as a parameter
 *
 * @see Konnect.delete
 */
fun delete(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().delete(url, requestBody, requestBuilder)
}

/**
 * Performs a PATCH request with Konnect default configurations
 *
 * @see Konnect.patch
 */
fun patch(url: String, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().patch(url, requestBuilder)
}

/**
 * Simplification of the patch() method where the body is supplied as a parameter
 *
 * @see Konnect.patch
 */
fun patch(url: String, requestBody: Any, requestBuilder: (RequestBuilder.() -> Unit)? = null): HttpResponse {
    return Konnect().patch(url, requestBody, requestBuilder)
}