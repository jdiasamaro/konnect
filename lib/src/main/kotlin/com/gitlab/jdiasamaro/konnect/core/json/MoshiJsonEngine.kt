package com.gitlab.jdiasamaro.konnect.core.json

import com.gitlab.jdiasamaro.konnect.KonnectJsonEngine
import com.gitlab.jdiasamaro.konnect.KonnectTypeContext
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Moshi.Builder
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.InputStream
import java.lang.reflect.Type
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZonedDateTime
import okio.buffer
import okio.source

internal class MoshiJsonEngine(customJsonAdapters: List<JsonAdapter.Factory> = emptyList()): KonnectJsonEngine {

    private val moshi: Moshi = Builder().apply {
        customJsonAdapters.forEach {
            add(it)
        }

        add(OffsetDateTime::class.java, ToStringBasedAdapter { OffsetDateTime.parse(it) })
        add(LocalDateTime::class.java, ToStringBasedAdapter { LocalDateTime.parse(it) })
        add(ZonedDateTime::class.java, ToStringBasedAdapter { ZonedDateTime.parse(it) })
        add(OffsetTime::class.java, ToStringBasedAdapter { OffsetTime.parse(it) })
        add(LocalDate::class.java, ToStringBasedAdapter { LocalDate.parse(it) })
        add(LocalTime::class.java, ToStringBasedAdapter { LocalTime.parse(it) })
        add(Instant::class.java, ToStringBasedAdapter { Instant.parse(it) })
        addLast(KotlinJsonAdapterFactory())
    }.build()

    override fun <T> toJson(value: T, typeContext: KonnectTypeContext<*>): String {
        val type = resolveTypeFromContext(typeContext)
        return moshi.adapter<T>(type).toJson(value)
    }

    override fun <T> fromJson(jsonInputStream: InputStream, typeContext: KonnectTypeContext<*>): T? {
        val type = resolveTypeFromContext(typeContext)

        return jsonInputStream.source().buffer().use {
            if (it.exhausted()) {
                null
            } else {
                moshi.adapter<T>(type).fromJson(it)
            }
        }
    }

    private fun resolveTypeFromContext(typeContext: KonnectTypeContext<*>): Type {
        return if (typeContext.typeParameters.isEmpty()) {
            typeContext.rawType
        } else {
            Types.newParameterizedType(typeContext.rawClass, *typeContext.typeParameters.toTypedArray())
        }
    }
}