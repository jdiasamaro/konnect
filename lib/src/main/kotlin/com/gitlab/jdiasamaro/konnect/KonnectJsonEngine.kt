package com.gitlab.jdiasamaro.konnect

import java.io.InputStream

/**
 * Expected behaviour for any JSON (de)serializer used by Konnect
 */
interface KonnectJsonEngine {
    fun <T> toJson(value: T, typeContext: KonnectTypeContext<*>): String
    fun <T> fromJson(jsonInputStream: InputStream, typeContext: KonnectTypeContext<*>): T?
}