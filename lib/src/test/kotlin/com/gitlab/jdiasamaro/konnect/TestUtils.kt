package com.gitlab.jdiasamaro.konnect

import assertk.Assert
import assertk.assertions.containsAll
import assertk.assertions.containsExactlyInAnyOrder
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.common.FileSource
import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer
import com.github.tomakehurst.wiremock.http.HttpHeader
import com.github.tomakehurst.wiremock.http.HttpHeaders
import com.github.tomakehurst.wiremock.http.QueryParameter
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import com.gitlab.jdiasamaro.konnect.configs.DEFLATE_ENCODING
import com.gitlab.jdiasamaro.konnect.core.json.MoshiJsonEngine
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONNECTION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_LENGTH
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.HOST
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.HTTP_2_SETTINGS
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.UPGRADE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.USER_AGENT
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZonedDateTime
import java.util.zip.Deflater

private val DEFAULT_HEADER_KEYS = listOf(
    HTTP_2_SETTINGS,
    CONTENT_LENGTH,
    CONNECTION,
    USER_AGENT,
    UPGRADE,
    HOST
)

internal val json = MoshiJsonEngine()

internal fun toJsonObject(jsonString: String): Map<String, Any?> {
    return json.fromJson(jsonString.byteInputStream(), buildReifiedTypeContext<Map<String, Any?>>())!!
}

internal fun jsonObject(map: Map<String, Any?>): String {
    return json.toJson(map, buildReifiedTypeContext<Map<String, Any?>>())
}

internal fun jsonArray(list: List<Any?>): String {
    return json.toJson(list, buildReifiedTypeContext<List<Any?>>())
}

internal fun Assert<HttpHeaders>.containsExactlyBesidesDefaultHeaders(vararg headers: HttpHeader) {
    given { httpHeaders ->
        val expectedHeaderKeys = DEFAULT_HEADER_KEYS union headers.map { it.key() }
        assertThat(httpHeaders.keys()).containsExactlyInAnyOrder(*expectedHeaderKeys.toTypedArray())
        assertThat(httpHeaders.all()).containsAll(*headers)
    }
}

internal fun Assert<Map<String, QueryParameter>>.containsExactly(vararg params: QueryParameter) {
    given { queryParams ->
        val paramKeys = params.map {
            it.key()
        }

        assertThat(queryParams.keys).containsExactlyInAnyOrder(*paramKeys.toTypedArray())
        params.forEach { param ->
            assertThat(queryParams.getValue(param.key())).given {
                assertThat(it.values()).containsExactlyInAnyOrder(*param.values().toTypedArray())
            }
        }
    }
}

internal data class ResponseEntity(
    val text: String,
    val int: Int,
    val long: Long,
    val bool: Boolean,
    val listOfWords: List<String>,
    val entities: Map<String, SimpleEntity>
)

internal data class SimpleEntity(
    val value: Double,
    val optional: String? = null
)

internal data class TemporalContainer(
    val offsetDateTime: OffsetDateTime = OffsetDateTime.now(),
    val localDateTime: LocalDateTime = LocalDateTime.now(),
    val zonedDateTime: ZonedDateTime = ZonedDateTime.now(),
    val offsetTime: OffsetTime = OffsetTime.now(),
    val localDate: LocalDate = LocalDate.now(),
    val localTime: LocalTime = LocalTime.now(),
    val instant: Instant = Instant.now(),
    val nullableInstant: Instant? = null
)

internal data class SimpleTemporalContainer(
    val instant: Instant
)

internal class HttpResponseDeflater: ResponseDefinitionTransformer() {

    override fun applyGlobally() = false

    override fun getName() = "deflater-interceptor"

    override fun transform(request: Request, responseDefinition: ResponseDefinition, files: FileSource, parameters: Parameters): ResponseDefinition {
        val acceptEncoding = request.headers.getHeader(ACCEPT_ENCODING).firstValue() ?: null

        return if (acceptEncoding.equals(DEFLATE_ENCODING)) {
            val compressedResponseBody = responseDefinition.byteBody.deflate()
            val headers = responseDefinition.headers ?: HttpHeaders.noHeaders()

            ResponseDefinitionBuilder()
                .withHeaders(headers.plus(HttpHeader(CONTENT_ENCODING, DEFLATE_ENCODING)))
                .withStatus(responseDefinition.status)
                .withBody(compressedResponseBody)
                .build()
        } else {
            responseDefinition
        }
    }
}

fun ByteArray.deflate(): ByteArray {
    val input = this

    val output = ByteArray(input.size * 4)
    val compressor = Deflater().apply {
        setInput(input)
        finish()
    }

    val compressedDataLength: Int = compressor.deflate(output)
    return output.copyOfRange(0, compressedDataLength)
}