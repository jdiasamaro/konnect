package com.gitlab.jdiasamaro.konnect

import assertk.assertThat
import assertk.assertions.containsAll
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.hasClass
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import assertk.assertions.isFalse
import assertk.assertions.isNull
import assertk.assertions.isTrue
import com.gitlab.jdiasamaro.konnect.configs.HttpRequestCompression.DEFLATE
import com.gitlab.jdiasamaro.konnect.configs.HttpRequestCompression.GZIP
import com.gitlab.jdiasamaro.konnect.core.KonnectException
import com.gitlab.jdiasamaro.konnect.core.KonnectExceptionType.DESERIALIZATION_ERROR
import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse
import com.gitlab.jdiasamaro.konnect.utils.HttpClientErrorStatusCode.UNPROCESSABLE_ENTITY
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpServerErrorStatusCode.INSUFFICIENT_STORAGE
import com.gitlab.jdiasamaro.konnect.utils.HttpServerErrorStatusCode.INTERNAL_SERVER_ERROR
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.NON_AUTHORITATIVE_INFORMATION
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.OK
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.BufferedReader
import java.io.InputStream
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZonedDateTime

class KonnectResponseParsingTest: BaseWiremockTest() {

    @Test
    fun `Konnect returns all headers sent in the response from the server`() {
        mockHttpResponse {
            statusCode = OK
            body = "this is the response data"
            header = "header1" to "1"
            header = "header2" to "2"
            header = "header3" to "3"
            header = "header3" to "4"
        }

        Konnect().get(endpoint).use { response ->
            val responseHeaders = response.headers

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseHeaders).containsAll(
                "header1" to listOf("1"),
                "header2" to listOf("2"),
                "header3" to listOf("3", "4")
            )
        }
    }

    @Test
    fun `Konnect returns an InputStream response object`() {
        mockHttpResponse {
            statusCode = OK
            body = "this is the response data"
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asStream().bufferedReader().use(BufferedReader::readText)

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseValue).isEqualTo("this is the response data")
        }
    }

    @Test
    fun `Konnect returns a String response object`() {
        mockHttpResponse {
            statusCode = OK
            body = "this is the response data"
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.status).isEqualTo(OK)
            assertThat(response.body.asText()).isEqualTo("this is the response data")
        }

        val response = Konnect().get(endpoint)
        val responseStreamVerifier = spyOnHttpResponseStream(response)

        assertThat(response.status).isEqualTo(OK)

        val responseText = response.toText()
        assertThat(responseText).isEqualTo("this is the response data")
        assertThat(responseStreamVerifier.wasClosed).isTrue()
    }

    @Test
    fun `Konnect returns an empty String when response stream is empty`() {
        mockHttpResponse {
            statusCode = OK
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.status).isEqualTo(OK)
            assertThat(response.body.asText()).isEmpty()
        }
    }

    @Test
    fun `Konnect returns an Int response object`() {
        mockHttpResponse {
            statusCode = OK
            body = "${Int.MIN_VALUE}"
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<Int>()

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseValue).isEqualTo(Int.MIN_VALUE)
        }
    }

    @Test
    fun `Konnect returns a Long response object`() {
        mockHttpResponse {
            statusCode = OK
            body = "${Long.MIN_VALUE}"
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<Long>()

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseValue).isEqualTo(Long.MIN_VALUE)
        }

        val response = Konnect().get(endpoint)
        val responseStreamVerifier = spyOnHttpResponseStream(response)

        assertThat(response.status).isEqualTo(OK)

        val responseLong = response.toValue<Long>()
        assertThat(responseLong).isEqualTo(Long.MIN_VALUE)
        assertThat(responseStreamVerifier.wasClosed).isTrue()
    }

    @Test
    fun `Konnect returns a Boolean response object`() {
        mockHttpResponse {
            statusCode = OK
            body = "True"
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<Boolean>()

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseValue).isEqualTo(true)
        }
    }

    @Test
    fun `Konnect returns a JSON response object`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonObject(mapOf(
                "text" to "this is a text",
                "int" to 12345,
                "long" to 897601,
                "bool" to true,
                "bigDecimal" to 2143443435,
                "listOfWords" to listOf("A", "AB", "ABC"),
                "entities" to mapOf(
                    "entity1" to mapOf(
                        "value" to 0.1,
                        "optional" to "some value"
                    ),
                    "entity2" to mapOf(
                        "value" to 0.2
                    )
                )
            ))
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.status).isEqualTo(OK)

            val responseValue = response.body.asValue<ResponseEntity>()
            assertThat(responseValue).isEqualTo(ResponseEntity(
                text = "this is a text",
                int = 12345,
                long = 897601,
                bool = true,
                listOfWords = listOf("A", "AB", "ABC"),
                entities = mapOf(
                    "entity1" to SimpleEntity(value = 0.1, optional = "some value"),
                    "entity2" to SimpleEntity(value = 0.2, optional = null)
                )
            ))
        }
    }

    @Test
    fun `Konnect returns a JSON array response object`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonArray(listOf(
                mapOf("value" to 0.1, "optional" to "value1"),
                mapOf("value" to 0.2),
                mapOf("value" to 0.3, "optional" to "value3"),
            ))
        }

        val expectedResult = listOf(
            SimpleEntity(value = 0.1, optional = "value1"),
            SimpleEntity(value = 0.2, optional = null),
            SimpleEntity(value = 0.3, optional = "value3")
        )

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<List<SimpleEntity>>()

            assertThat(response.status).isEqualTo(OK)
            assertThat(responseValue).containsExactlyInAnyOrder(*expectedResult.toTypedArray())
        }
    }

    @Test
    fun `Konnect handles empty responses correctly using nullable type`() {
        mockHttpResponse {
            statusCode = OK
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.status).isEqualTo(OK)

            val responseValue = assertThat {
                response.body.asValue<SimpleEntity>()
            }

            responseValue.isFailure().hasClass(KonnectException::class.java)
        }
    }

    @Test
    fun `Konnect response knows when it is a 2XX success`() {
        mockHttpResponse {
            statusCode = NON_AUTHORITATIVE_INFORMATION
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.isSuccess()).isTrue()
            assertThat(response.isRedirect()).isFalse()
            assertThat(response.isClientError()).isFalse()
            assertThat(response.isServerError()).isFalse()
        }
    }

    @Test
    fun `Konnect response knows when it is a 4XX client error`() {
        mockHttpResponse {
            statusCode = UNPROCESSABLE_ENTITY
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.isSuccess()).isFalse()
            assertThat(response.isRedirect()).isFalse()
            assertThat(response.isClientError()).isTrue()
            assertThat(response.isServerError()).isFalse()
        }
    }

    @Test
    fun `Konnect response knows when it is a 5XX server error`() {
        mockHttpResponse {
            statusCode = INSUFFICIENT_STORAGE
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.isSuccess()).isFalse()
            assertThat(response.isRedirect()).isFalse()
            assertThat(response.isClientError()).isFalse()
            assertThat(response.isServerError()).isTrue()
        }
    }

    @Test
    fun `Konnect response handles HTTP errors with a response body`() {
        mockHttpResponse {
            statusCode = INTERNAL_SERVER_ERROR
            body = jsonObject(mapOf(
                "value" to 76431,
                "optional" to "unexpected error"
            ))
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<SimpleEntity>()

            assertThat(response.isServerError()).isTrue()
            assertThat(responseValue).isEqualTo(SimpleEntity(
                value = 76431.0,
                optional = "unexpected error"
            ))
        }
    }

    @Test
    fun `Konnect handles JSON object as response type`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonObject(mapOf(
                "value" to 0.1,
                "nullOptional" to null,
                "optional" to "some value",
                "otherValue" to 9.123
            ))
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asJsonObject()

            assertThat(response.isSuccess()).isTrue()
            assertThat(responseValue.keys).containsExactlyInAnyOrder("value", "optional", "otherValue")
            assertThat(responseValue["value"]).isEqualTo(0.1)
            assertThat(responseValue["otherValue"]).isEqualTo(9.123)
            assertThat(responseValue["optional"]).isEqualTo("some value")
        }

        val response = Konnect().get(endpoint)
        val responseStreamVerifier = spyOnHttpResponseStream(response)

        assertThat(response.isSuccess()).isTrue()

        val responseJsonObject = response.toJsonObject()
        assertThat(responseJsonObject.keys).containsExactlyInAnyOrder("value", "optional", "otherValue")
        assertThat(responseJsonObject["value"]).isEqualTo(0.1)
        assertThat(responseJsonObject["otherValue"]).isEqualTo(9.123)
        assertThat(responseJsonObject["optional"]).isEqualTo("some value")
        assertThat(responseStreamVerifier.wasClosed).isTrue()
    }

    @Test
    fun `Konnect handles JSON array as response type`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonArray(listOf(
                1.234, "false", null, true, SimpleEntity(value = 2.5, optional = "non-null"), SimpleEntity(value = 2.5)
            ))
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asJsonArray()

            assertThat(response.isSuccess()).isTrue()
            assertThat(responseValue[0]).isEqualTo(1.234)
            assertThat(responseValue[1]).isEqualTo("false")
            assertThat(responseValue[2]).isEqualTo(null)
            assertThat(responseValue[3]).isEqualTo(true)
            assertThat(responseValue[4]).isEqualTo(mapOf("value" to 2.5, "optional" to "non-null"))
            assertThat(responseValue[5]).isEqualTo(mapOf("value" to 2.5))
        }

        val response = Konnect().get(endpoint)
        val responseStreamVerifier = spyOnHttpResponseStream(response)

        assertThat(response.isSuccess()).isTrue()

        val responseJsonArray = response.toJsonArray()
        assertThat(responseJsonArray[0]).isEqualTo(1.234)
        assertThat(responseJsonArray[1]).isEqualTo("false")
        assertThat(responseJsonArray[2]).isEqualTo(null)
        assertThat(responseJsonArray[3]).isEqualTo(true)
        assertThat(responseJsonArray[4]).isEqualTo(mapOf("value" to 2.5, "optional" to "non-null"))
        assertThat(responseJsonArray[5]).isEqualTo(mapOf("value" to 2.5))
        assertThat(responseStreamVerifier.wasClosed).isTrue()
    }

    @Test
    fun `Konnect throws an error when a deserialization problem happens on a custom JSON adapter`() {
        mockHttpResponse {
            statusCode = OK
            body = "123456"
        }

        val problematicJsonFactory = JsonAdapter.Factory { type, _, _ ->
            if (type == SimpleEntity::class.java) {
                object: JsonAdapter<SimpleEntity>() {
                    override fun fromJson(reader: JsonReader): SimpleEntity? {
                        throw IndexOutOfBoundsException()
                    }

                    override fun toJson(writer: JsonWriter, value: SimpleEntity?) {
                        throw IndexOutOfBoundsException()
                    }
                }
            } else {
                null
            }
        }

        Konnect {
            jsonAdapters {
                add(problematicJsonFactory)
            }
        }.get(endpoint).use { response ->
            val result = assertThat {
                response.body.asValue<SimpleEntity>()
            }

            result.isFailure().given {
                val error = it as KonnectException
                assertThat(error.type).isEqualTo(DESERIALIZATION_ERROR)
                assertThat(error.cause!!::class.java).isEqualTo(IndexOutOfBoundsException::class.java)
            }
        }
    }

    @Test
    fun `Konnect is able to deserialize common Java temporal instances`() {
        val temporalMap = mapOf(
            "offsetDateTime" to OffsetDateTime.now(),
            "localDateTime" to LocalDateTime.now(),
            "zonedDateTime" to ZonedDateTime.now(),
            "offsetTime" to OffsetTime.now(),
            "localDate" to LocalDate.now(),
            "localTime" to LocalTime.now(),
            "instant" to Instant.now()
        )

        mockHttpResponse {
            statusCode = OK
            body = jsonObject(temporalMap)
        }

        Konnect().get(endpoint).use { response ->
            val responseValue = response.body.asValue<TemporalContainer>()

            assertThat(response.isSuccess()).isTrue()
            assertThat(responseValue.offsetDateTime).isEqualTo(temporalMap["offsetDateTime"])
            assertThat(responseValue.localDateTime).isEqualTo(temporalMap["localDateTime"])
            assertThat(responseValue.zonedDateTime).isEqualTo(temporalMap["zonedDateTime"])
            assertThat(responseValue.offsetTime).isEqualTo(temporalMap["offsetTime"])
            assertThat(responseValue.localDate).isEqualTo(temporalMap["localDate"])
            assertThat(responseValue.localTime).isEqualTo(temporalMap["localTime"])
            assertThat(responseValue.instant).isEqualTo(temporalMap["instant"])
            assertThat(responseValue.nullableInstant).isNull()
        }
    }

    @Test
    fun `Konnect throws an error when deserializing a wrong temporal format`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonObject(mapOf(
                "instant" to 12345
            ))
        }

        Konnect().get(endpoint).use { response ->
            assertThat(response.isSuccess()).isTrue()

            val responseValue = assertThat {
                response.body.asValue<SimpleTemporalContainer>()
            }

            responseValue.isFailure().given {
                val error = it as KonnectException
                assertThat(error.type).isEqualTo(DESERIALIZATION_ERROR)
                assertThat(error.cause!!::class.java).isEqualTo(JsonDataException::class.java)
            }
        }
    }

    @Test
    fun `Konnect handles GZIP response streams seamlessly`() {
        mockHttpResponse {
            statusCode = OK
            body = jsonObject(mapOf(
                "value" to 123,
                "boolean" to true
            ))
        }

        Konnect().get(endpoint) {
            compression accept GZIP
        }.use { response ->
            val responseValue = response.body.asJsonObject()
            assertThat(response.isSuccess()).isTrue()
            assertThat(response.headers[CONTENT_ENCODING]).isEqualTo(listOf("gzip"))

            assertThat(responseValue.keys).containsExactlyInAnyOrder("value", "boolean")
            assertThat(responseValue["value"]).isEqualTo(123.0)
            assertThat(responseValue["boolean"]).isEqualTo(true)
        }
    }

    @Test
    fun `Konnect handles Deflate response streams seamlessly`() {
        mockDeflateHttpResponse {
            statusCode = OK
            body = jsonObject(mapOf(
                "value" to 123,
                "boolean" to true
            ))
        }

        Konnect().get(endpoint) {
            compression accept DEFLATE
        }.use { response ->
            val responseValue = response.body.asJsonObject()
            assertThat(response.isSuccess()).isTrue()
            assertThat(response.headers[CONTENT_ENCODING]).isEqualTo(listOf("deflate"))

            assertThat(responseValue.keys).containsExactlyInAnyOrder("value", "boolean")
            assertThat(responseValue["value"]).isEqualTo(123.0)
            assertThat(responseValue["boolean"]).isEqualTo(true)
        }
    }
}

private fun spyOnHttpResponseStream(response: HttpResponse): VerifiableInputStream {
    val httpResponseField = response.javaClass.getDeclaredField("internal").apply {
        isAccessible = true
    }

    val realHttpResponse = httpResponseField.get(response) as java.net.http.HttpResponse<*>
    val realInputStream = realHttpResponse.body() as InputStream

    val verifiableInputStream = VerifiableInputStream(realInputStream)
    val mockedHttpResponse = mock<java.net.http.HttpResponse<InputStream>>()
    whenever(mockedHttpResponse.body()).thenReturn(verifiableInputStream)

    httpResponseField.set(response, mockedHttpResponse)
    return verifiableInputStream
}

class VerifiableInputStream(private val inputStream: InputStream): InputStream() {
    var wasClosed: Boolean = false

    override fun read(): Int {
        return inputStream.read()
    }

    override fun available(): Int {
        return inputStream.available()
    }

    override fun close() {
        inputStream.close()
        wasClosed = true
    }
}