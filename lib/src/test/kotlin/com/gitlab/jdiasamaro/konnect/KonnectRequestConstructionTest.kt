package com.gitlab.jdiasamaro.konnect

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNullOrEmpty
import assertk.assertions.isTrue
import assertk.assertions.startsWith
import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.client.WireMock.deleteRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.patchRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.putRequestedFor
import com.github.tomakehurst.wiremock.http.HttpHeader.httpHeader
import com.github.tomakehurst.wiremock.http.QueryParameter.queryParam
import com.gitlab.jdiasamaro.konnect.configs.GZIP_ENCODING
import com.gitlab.jdiasamaro.konnect.configs.HttpRequestCompression.GZIP
import com.gitlab.jdiasamaro.konnect.configs.HttpRequestCompression.NO_COMPRESSION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT_ENCODING
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_LENGTH
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.USER_AGENT
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.OK
import org.junit.Test

class KonnectRequestConstructionTest: BaseWiremockTest() {

    @Test
    fun `Konnect sends out all headers and query parameters`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        get(endpoint) {
            headers {
                header("X-Header-1") set "value1"

                header("X-Header-2") add "value2"
                header("X-Header-2") set "value2_1"
                header("X-Header-2") add "value2_2"

                header("X-Header-3") add listOf("value3_1", "value3_2")
            }

            queryParameters {
                param("param1") set "p1"

                param("param2") add "p2"
                param("param2") set "p2_1"
                param("param2") add "p2_2"

                param("param3") add listOf("p3_1", "p3_2")
            }
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(headers).containsExactlyBesidesDefaultHeaders(
                    httpHeader(CONTENT_LENGTH, "0"),
                    httpHeader(USER_AGENT, "Konnect/${konnectVersion}"),
                    httpHeader("X-Header-1", "value1"),
                    httpHeader("X-Header-2", "value2_1", "value2_2"),
                    httpHeader("X-Header-3", "value3_1", "value3_2")
                )

                assertThat(queryParams).containsExactly(
                    queryParam("param1", "p1"),
                    queryParam("param2", "p2_1", "p2_2"),
                    queryParam("param3", "p3_1", "p3_2")
                )

                assertThat(body).isNullOrEmpty()
            }
        }
    }

    @Test
    fun `Konnect is able to send a body on a GET request`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect().get(endpoint, "this is a request").use {
            assertThat(it.isSuccess()).isTrue()
            assertThat(it.toText()).isEqualTo("data")

            verify(getRequestedFor(anyUrl())) {
                assertThat(bodyAsString).isEqualTo("this is a request")
            }
        }
    }

    @Test
    fun `Konnect preserves query parameters already on the supplied URL`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        get("${endpoint}?param1=p1_1&param1=p1_2&param2=p2_1&param3=p3") {
            queryParameters {
                param("param1") add "p1_3"
                param("param2") add "p2_2"
                param("param3") set listOf("p3_1", "p3_2")
            }
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("param1", "p1_1", "p1_2", "p1_3"),
                    queryParam("param2", "p2_1", "p2_2"),
                    queryParam("param3", "p3", "p3_1", "p3_2")
                )
            }
        }
    }

    @Test
    fun `Konnect respects encoded query parameters on the supplied URL`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        delete("${endpoint}?file=%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(deleteRequestedFor(anyUrl())) {
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("file", "another_file.pdf", "%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using both a valid base URL and query-only extra URL`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/base"
        }.patch("?file=%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(patchRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).startsWith("${endpoint}/base?")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("file", "another_file.pdf", "%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using both a valid base URL and complex extra URL`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/first/base/"
        }.put("/second/base?file=%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(putRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).startsWith("${endpoint}/first/base/second/base?")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("file", "another_file.pdf", "%2FSimplestProc%2FProcess%20Packages%2FSimplestProc.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using base and extra URL both with query parameters`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/first/base/?baseParam=base"
        }.get("/second/base/?extraParam=extra") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).startsWith("${endpoint}/first/base/second/base/?")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("baseParam", "base"),
                    queryParam("extraParam", "extra"),
                    queryParam("file", "another_file.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using base URL and extra URL with empty query string`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/first/base/?baseParam=base"
        }.post("/second/base/?") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(postRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).startsWith("${endpoint}/first/base/second/base/?")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("baseParam", "base"),
                    queryParam("file", "another_file.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using base URL with empty query string`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/first/base/?"
        }.get("/second/base?extraParam=extra") {
            queryParameters {
                param("file") add "another_file.pdf"
            }
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).startsWith("${endpoint}/first/base/second/base?")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(body).isNullOrEmpty()

                assertThat(queryParams).containsExactly(
                    queryParam("extraParam", "extra"),
                    queryParam("file", "another_file.pdf")
                )
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using base URL with no query params`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = "${endpoint}/first/base"
        }.delete("/second/base").use {
            verify(deleteRequestedFor(anyUrl())) {
                assertThat(absoluteUrl).isEqualTo("${endpoint}/first/base/second/base")
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(queryParams).isNullOrEmpty()
                assertThat(body).isNullOrEmpty()
            }
        }
    }

    @Test
    fun `Konnect builds request URL correctly using base URL only with host (no base or extra paths)`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect {
            baseUrl = endpoint
        }.get("").use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(absoluteUrl.removeSuffix("/")).isEqualTo(endpoint)
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(queryParams).isNullOrEmpty()
                assertThat(body).isNullOrEmpty()
            }
        }
    }

    @Test
    fun `Konnect allows requests to be build with accepted encoding header parameters`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        Konnect().get(endpoint) {
            compression accept GZIP
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(absoluteUrl.removeSuffix("/")).isEqualTo(endpoint)
                assertThat(headers).containsExactlyBesidesDefaultHeaders(
                    httpHeader(ACCEPT_ENCODING, GZIP_ENCODING)
                )
                assertThat(queryParams).isNullOrEmpty()
                assertThat(body).isNullOrEmpty()
            }
        }

        Konnect().get(endpoint) {
            compression accept listOf(NO_COMPRESSION)
        }.use {
            verify(getRequestedFor(anyUrl())) {
                assertThat(absoluteUrl.removeSuffix("/")).isEqualTo(endpoint)
                assertThat(headers).containsExactlyBesidesDefaultHeaders()
                assertThat(queryParams).isNullOrEmpty()
                assertThat(body).isNullOrEmpty()
            }
        }
    }
}