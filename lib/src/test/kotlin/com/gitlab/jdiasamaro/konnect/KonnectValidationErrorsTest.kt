package com.gitlab.jdiasamaro.konnect

import assertk.all
import assertk.assertThat
import assertk.assertions.cause
import assertk.assertions.hasClass
import assertk.assertions.isFailure
import assertk.assertions.matchesPredicate
import com.gitlab.jdiasamaro.konnect.core.KonnectException
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.OK
import java.net.URISyntaxException
import org.junit.Test

class KonnectValidationErrorsTest: BaseWiremockTest() {

    @Test
    fun `Konnect reports error when blank URL is supplied`() {
        var result = assertThat {
            put("")
        }

        result.isFailure().hasClass(IllegalArgumentException::class.java)

        result = assertThat {
            put(" ", "some body text")
        }

        result.isFailure().hasClass(IllegalArgumentException::class.java)
    }

    internal fun test(): List<SimpleEntity>? {
        return get("some body text").use {
            if (it.status == 404) {
                it.body.asValue()
            } else {
                null
            }
        }
    }

    @Test
    fun `Konnect reports an error if supplied query string is not URL-encoded`() {
        mockHttpResponse {
            statusCode = OK
            body = "data"
        }

        val result = assertThat {
            delete("${endpoint}?file=%2FSimplest&Proc%2FProcess%20Packages%2FSimplestProc.pdf", "some body text")
        }

        result.isFailure().all {
            hasClass(KonnectException::class.java)
            cause().matchesPredicate {
                it?.javaClass == URISyntaxException::class.java
            }
        }
    }
}