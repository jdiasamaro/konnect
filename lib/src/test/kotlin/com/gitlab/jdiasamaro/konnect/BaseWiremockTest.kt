package com.gitlab.jdiasamaro.konnect

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder
import com.github.tomakehurst.wiremock.verification.LoggedRequest
import com.marcinziolo.kotlin.wiremock.SpecifyResponse
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.like
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.returns
import org.junit.After
import org.junit.Before
import java.net.ServerSocket

open class BaseWiremockTest {

    internal val konnectVersion = Konnect::class.java.classLoader.getResource("version")!!.readText()

    private val testPort = ServerSocket(0).use {
        it.localPort
    }

    private val wiremock = WireMockServer(options()
        .extensions(HttpResponseDeflater::class.java)
        .notifier(ConsoleNotifier(true))
        .port(testPort))

    internal val endpoint = "http://localhost:${testPort}"

    @Before
    fun setup() {
        wiremock.start()
    }

    @After
    fun tearDown() {
        wiremock.resetAll()
        wiremock.stop()
    }

    internal fun mockHttpResponse(mock: SpecifyResponse) {
        wiremock.get {
            url like "/.*"
        } returns mock

        wiremock.post {
            url like "/.*"
        } returns mock
    }

    internal fun mockDeflateHttpResponse(mock: SpecifyResponse) {
        val mockSetup = wiremock.get {
            url like "/.*"
        }

        mockSetup.responseDefinitionBuilder.withTransformers("deflater-interceptor")

        mockSetup returns mock
    }

    internal fun verify(patternBuilder: RequestPatternBuilder, block: LoggedRequest.() -> Unit) {
        val request = wiremock.findAll(patternBuilder).last()
        block.invoke(request)
    }
}