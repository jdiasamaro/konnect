package com.gitlab.jdiasamaro.konnect

import assertk.assertThat
import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.http.HttpHeader.httpHeader
import com.github.tomakehurst.wiremock.http.QueryParameter.queryParam
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.AUTHORIZATION
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_LANGUAGE
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_TYPE
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.OK
import org.junit.Test

class KonnectConfigurationTest: BaseWiremockTest() {

    private val konnect = Konnect {
        defaults {
            headers {
                authorization set "Bearer dn32od823hduAod3p23021231"
            }

            queryParameters {
                param("common") set 12345
            }
        }
    }

    @Test
    fun `Konnect always sends default values along with all requests`() {
        mockHttpResponse {
            statusCode = OK
        }

        konnect.post("${endpoint}/first") {
            headers {
                contentType set "application/test"
            }

            queryParameters {
                param("first") set 0.1
            }
        }.close()

        verify(postRequestedFor(anyUrl())) {
            assertThat(headers).containsExactlyBesidesDefaultHeaders(
                httpHeader(AUTHORIZATION, "Bearer dn32od823hduAod3p23021231"),
                httpHeader(CONTENT_TYPE, "application/test")
            )

            assertThat(queryParams).containsExactly(
                queryParam("common", "12345"),
                queryParam("first", "0.1")
            )
        }
    }

    @Test
    fun `Konnect always creates a new set of defaults to use even if instance is the same`() {
        mockHttpResponse {
            statusCode = OK
        }

        konnect.get("${endpoint}/second") {
            headers {
                contentLanguage set "por"
            }

            queryParameters {
                param("second") set 0.2
            }
        }.close()

        verify(getRequestedFor(anyUrl())) {
            assertThat(headers).containsExactlyBesidesDefaultHeaders(
                httpHeader(AUTHORIZATION, "Bearer dn32od823hduAod3p23021231"),
                httpHeader(CONTENT_LANGUAGE, "por")
            )

            assertThat(queryParams).containsExactly(
                queryParam("common", "12345"),
                queryParam("second", "0.2")
            )
        }
    }

    @Test
    fun `Konnect allows defaults to be overwritten`() {
        mockHttpResponse {
            statusCode = OK
        }

        konnect.get("${endpoint}/second") {
            headers {
                authorization set "Basic 123456"
                contentLanguage set "eng"
            }

            queryParameters {
                param("common") set true
                param("other") set "another param"
            }
        }.close()

        verify(getRequestedFor(anyUrl())) {
            assertThat(headers).containsExactlyBesidesDefaultHeaders(
                httpHeader(AUTHORIZATION, "Basic 123456"),
                httpHeader(CONTENT_LANGUAGE, "eng")
            )

            assertThat(queryParams).containsExactly(
                queryParam("common", "true"),
                queryParam("other", "another param")
            )
        }
    }
}