package com.gitlab.jdiasamaro.konnect

import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.client.WireMock.patchRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.CONTENT_TYPE
import com.gitlab.jdiasamaro.konnect.utils.HttpSuccessStatusCode.OK
import org.junit.Test

class KonnectRequestBodyTest: BaseWiremockTest() {

    @Test
    fun `Konnect is able to publish a String body and infer the Content-Type automatically`() {
        mockHttpResponse {
            statusCode = OK
        }

        patch(endpoint, "This is some text").use {
            verify(patchRequestedFor(anyUrl())) {
                assertThat(headers.getHeader(CONTENT_TYPE).values()).containsExactly("text/plain; charset=UTF-8")
                assertThat(bodyAsString).isEqualTo("This is some text")
            }
        }
    }

    @Test
    fun `Konnect is able to publish a byte array body and infer the Content-Type automatically`() {
        mockHttpResponse {
            statusCode = OK
        }

        post(endpoint, "This is some text from a byte array".toByteArray()).use {
            verify(postRequestedFor(anyUrl())) {
                assertThat(headers.getHeader(CONTENT_TYPE).values()).containsExactly("application/octet-stream")
                assertThat(bodyAsString).isEqualTo("This is some text from a byte array")
            }
        }
    }

    @Test
    fun `Konnect is able to publish a body from an input stream and infer the Content-Type automatically`() {
        mockHttpResponse {
            statusCode = OK
        }

        post(endpoint) {
            body { "This is some text from an input stream".byteInputStream() }
        }.use {
            verify(postRequestedFor(anyUrl())) {
                assertThat(headers.getHeader(CONTENT_TYPE).values()).containsExactly("application/octet-stream")
                assertThat(bodyAsString).isEqualTo("This is some text from an input stream")
            }
        }
    }

    @Test
    fun `Konnect is able to publish a JSON body and infer the Content-Type automatically`() {
        mockHttpResponse {
            statusCode = OK
        }

        patch(endpoint) {
            body { SimpleEntity(value = 0.1, optional = "value1") }
        }.use {
            verify(patchRequestedFor(anyUrl())) {
                assertThat(headers.getHeader(CONTENT_TYPE).values()).containsExactly("application/json; charset=UTF-8")
                val deserializedJsonObject = toJsonObject(bodyAsString)

                assertThat(deserializedJsonObject.keys).containsExactlyInAnyOrder("value", "optional")
                assertThat(deserializedJsonObject["value"]).isEqualTo(0.1)
                assertThat(deserializedJsonObject["optional"]).isEqualTo("value1")
            }
        }
    }

    @Test
    fun `Konnect uses supplied Content-Type header even if inferred type is different`() {
        mockHttpResponse {
            statusCode = OK
        }

        post(endpoint) {
            body { SimpleEntity(value = 0.1, optional = "value1") }
            headers { contentType set "image/jpeg" }
        }.use {
            verify(postRequestedFor(anyUrl())) {
                assertThat(headers.getHeader(CONTENT_TYPE).values()).containsExactly("image/jpeg")
                val deserializedJsonObject = toJsonObject(bodyAsString)

                assertThat(deserializedJsonObject.keys).containsExactlyInAnyOrder("value", "optional")
                assertThat(deserializedJsonObject["value"]).isEqualTo(0.1)
                assertThat(deserializedJsonObject["optional"]).isEqualTo("value1")
            }
        }
    }
}