<img src="https://gitlab.com/jdiasamaro/konnect/uploads/983e164ac0a3267d76a7b9bdc1b65616/konnect.png" alt="konnect" width="150">

[![](https://jitpack.io/v/com.gitlab.jdiasamaro/konnect.svg)](https://jitpack.io/#com.gitlab.jdiasamaro/konnect)
[![](https://gitlab.com/jdiasamaro/konnect/badges/main/pipeline.svg)](https://gitlab.com/jdiasamaro/konnect/-/commits/main)

HTTP should be simple, and Konnect is a simple and idiomatic HTTP client for Kotlin. It stays of out the way when it comes to the little details of building web requests and intends to be straight to the point.

Konnect lives by "convention over configuration" and its purpose is to relieve the client of complex configurations. If it can be automated or even simply customized, Konnect will allow you to do just that.

# Getting started

### Get a generic JSON array with all users
```kotlin
// JSON arrays are simply lists
val users = get("api.com/v2/users").toJsonArray()
```

### Create a new user by POSTing a simple JSON object
```kotlin
// JSON objects are maps of String to anything
val userData = mapOf(
    "id" to 1234,
    "active" to true,
    "name" to "Jon Snow",
    "city" to "The wall in the North",
    "occupations" to listOf("Black Knight", "Stark"),
)

post("api.com/v2/users", userData).use { response ->
    when {
        response.isSuccess() -> println("User created!")
        response.isServerError() -> throw IllegalStateException("Server returned an error")
        response.status == CONFLICT -> throw UserAlreadyExistsException(name = userData.name)
    }
}
```

### Receiving a response as a typed value
```kotlin
data class ServerInfo(val id: String, val now: Instant)

fun getServerInfo(): ServerInfo? {
    return get("api.com/v2/server-info").use { response ->
        if (response.isSuccess()) {
            response.body.asValue()
        } else {
            null
        }
    }
}
```

### Configuring default values to be used on all requests
```kotlin
val api = Konnect {
    baseUrl = "api.com/v2/"
    defaults {
        headers {
            authorization set "Bearer zaCELgL0imfnc8mVLWwsAawjYr4Rx-Af50DDqtlx"
        }
    }
}

// returns a list of deserialized JSON user values
// request is sent to api.com/v2/users and with the authorization header
fun getUsers(): List<User> {
    return api.get("/users").toValue()
}
```

### Fire-and-forget requests
```kotlin
fun createUser(user: User) {
    // Here the method ignores the result of the POST request
    // Why would you even want to do that? You can though, just close the response to
    // avoid leaking your application resources
    api.post("/users", user).close()
}
```

## Installation (gradle)

Konnect binaries are hosted by Jitpack. Please add Jitpack to the list of project repositories
```kotlin
repositories {
    maven {
      url = uri("https://jitpack.io")
    }
}
```

Then add Konnect to the project dependencies:
```kotlin
implementation("com.gitlab.jdiasamaro:konnect:0.21.0")
```

All available library versions are listed here: https://jitpack.io/#com.gitlab.jdiasamaro/konnect

# Features

* Fluent Kotlin DSL
* Request URLs can be composed with base URL parts
* JSON response deserialization
* Support for default JSON objects and arrays without defining static types (using `Map<String, Any?>` and `List<Any?>`)
* Support for Kotlin nullable types as response values
* Automatic handling of GZip and Deflate `Content-Encoding` response headers (simple or combined)